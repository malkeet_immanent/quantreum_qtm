require('babel-register')
var HDWalletProvider = require("truffle-hdwallet-provider");

// Get our mnemonic and create an hdwallet
var mnemonicRopsten = "This is my menmonic key to place inside truffle.js";
var mnemonicMainnet = "";
var providerUrl = "https://ropsten.infura.io/Hud4v5YCDxUeXkGTJ4Dw";
var providerUrlMainNet = "https://mainnet.infura.io/Hud4v5YCDxUeXkGTJ4Dw";

var providerUrlkovan = "https://kovan.infura.io/Hud4v5YCDxUeXkGTJ4Dw";

module.exports = {
  networks: {
    "ropsten": {
      network_id: 3,    // Official ropsten network id
      provider: new HDWalletProvider(mnemonicRopsten, providerUrl), // Use our custom provider
      gas: 3000000
     // from: address     // Use the address we derived
    },
    "kovan": {
      network_id: 3,    // Official ropsten network id
      provider: new HDWalletProvider(mnemonicRopsten, providerUrlkovan), // Use our custom provider
     // from: address     // Use the address we derived
    },
    "live": {
      network_id: 1,
      provider: new HDWalletProvider(mnemonicMainnet, providerUrlMainNet),
    },
    "development": {
      host: 'localhost',
      port: 8545,
      network_id: '*', // Match any network id
      gas: 4000000
    }
  }
};
