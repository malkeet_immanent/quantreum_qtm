var QtmToken = artifacts.require("./QtmToken.sol");
var QtmCrowdFund = artifacts.require("./QtmCrowdFund.sol");

module.exports = function(deployer) {
    deployer.
    deploy(QtmCrowdFund,"0xd211B86fd555aA31C4572d0150C2AF2d7EbF6513")
    .then(()=> {
         deployer.
          deploy(QtmToken, "0xd211B86fd555aA31C4572d0150C2AF2d7EbF6513",QtmCrowdFund.address);
    });
};
