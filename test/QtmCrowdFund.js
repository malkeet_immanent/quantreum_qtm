var QtmToken = artifacts.require("./QtmToken.sol");
var QtmCrowdFund = artifacts.require("./QtmCrowdFund.sol");
var BigNumber = require('bignumber.js');


contract(QtmCrowdFund , function(accounts){

var founderMultiSigAddress = accounts[1];
var reaminingTokenHolder = accounts[7];
var tokenAddress = "0x358e3db13e7e84a6bb03397407bcb7e70f3a0ccc";
var owner = accounts[5];



// it("intialization of the crowdfund with founder address , remaining token holder address",function(done){
//      var now = Date.now();
//      var crowdsale;
//     QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         return crowdsale.founderMultiSigAddress.call();
//     }).then(function(address){
//         assert.strictEqual(address,founderMultiSigAddress);
//         return true;  
//     }).then(function(result){
//         return crowdsale.setTokenAddress(tokenAddress,{from: founderMultiSigAddress , gas:150000})
//     }).then(function(result){
//         return crowdsale.startCrowdfund(2000,45,{from: founderMultiSigAddress , gas:150000});
//     }).then(function(result){
//         return crowdsale.crowdfundEndTime.call();
//     }).then(function(result){
//         assert.strictEqual(Math.floor(now/1000 + 45*24*60*60), result.toNumber());
//         done();
//     }).catch(done);
// });

//  // Test cases for setFounderMultiSigAddress 

// it("setFounderMultiSigAddress : Should change the founder multi signature address",function(done){
//     var crowdsale;
//      QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         return instance;
//     }).then(function(result){
//         crowdsale.setFounderMultiSigAddress(accounts[6],{from : founderMultiSigAddress , gas:1500000}).then(function(tx){
//             return tx;
//         }).then(function(result){
//             return crowdsale.founderMultiSigAddress.call();
//         }).then(function(account){
//             assert.strictEqual(account,accounts[6]);
//             done();
//         }).catch(done);
//     });
// });

// it("setFounderMultiSigAddress : Attempt to change the founder multi signature address by other then owner -- fails",function(){
//    var crowdsale;
//      QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         return instance;
//     }).then(function(result){
//         crowdsale.setFounderMultiSigAddress(accounts[6],{from : accounts[7] , gas:1500000}).then(function(tx){
//         return tx;
//     }).then(function(result){
//         assert(false, "it expected to return exception but it didn't");
//     }).catch(function(error){
//         if(error.toString().indexOf("invalid opcode") != -1) {
//          console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to check the call other than owner");
//          assert(true,true);
//          }else{
//          assert(false,error.toString());
//         }
//     });
//   });
// });


// Test cases for setTokenAddress

it("setTokenAddress: should set token address in crowdfund contract", function(done){
     var crowdsale;
     QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        return instance;
     }).then(function(result){
         crowdsale.setTokenAddress(tokenAddress,{from:founderMultiSigAddress, gas:150000}).then(function(tx){
             return tx;
         });
         // .then(function(result){
         //     crowdsale.startCrowdfund(2000,45,{from:founderMultiSigAddress, gas:150000}).then(function(state){
         //         return state;
         //     }).then(function(result){
         //         return crowdsale.isCrowdFundActive.call();
         //     }).then(function(status){
         //         assert.isTrue(status);
         //         done();
         //     }).catch(done);
         // });
     });
});


// it("setTokenAddress : Attempt to set token address in crowdfund contract by another address instead of founder address -- fail ", function(){
//      var crowdsale;
//      QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         return instance;
//      }).then(function(result){
//          crowdsale.setTokenAddress(tokenAddress,{from:accounts[8], gas:150000}).then(function(tx){
//              return tx;
//          }).then(function(result){
//             assert(false, "it expected to return exception but it didn't");
//     }).catch(function(error){
//         if(error.toString().indexOf("invalid opcode") != -1) {
//          console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to check the call other than owner");
//          assert(true,true);
//          }else{
//          assert(false,error.toString());
//         }
//     });
//   });
// });

// // // ============================================================================
 
//  it("setTokenAddress : Should NOT let a founder address to set the token address when token is set", function(done) {
//         var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress,{from:founderMultiSigAddress, gas:150000});
//         }).then(() => {
//             return crowdsale.setTokenAddress(tokenAddress,{from:founderMultiSigAddress, gas:150000});
//         }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });

//     // // Test case for changeCrowdfundState

// it(" changeCrowdfundState :Should NOT let a random address activate the crowdfund", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.token.call();
//         }).then(function (result) {
//             assert.strictEqual(result, tokenAddress);
//             return crowdsale.changeCrowdfundState({from: accounts[8], gas:150000});
//         }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });



//     it("changeCrowdfundState :Not let the founder address to activate the crowdfund if token is not set", function(done) {
//        var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.changeCrowdfundState({from: founderMultiSigAddress, gas:150000});
//          }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });


//     it(" changeCrowdfundState :crowdfund should be deactivated by addresss after calling startCrowdFund()", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.token.call();
//         }).then(function (result) {
//             assert.strictEqual(result, tokenAddress);
//             return crowdsale.startCrowdfund(2000,45,{from:founderMultiSigAddress , gas:150000});
//         }).then(function(result){
//                 return crowdsale.isCrowdFundActive.call();
//             }).then(function(result){
//                 assert.isTrue(result);
//                 return crowdsale.changeCrowdfundState({from : founderMultiSigAddress , gas:150000});
//             }).then(function(result){
//                 return crowdsale.isCrowdFundActive.call();
//             }).then(function(result){
//                 assert.isFalse(result);
//             done();
//         }).catch(done);
//     });


//      it(" changeCrowdfundState :crowdfund should be deactivated by addresss before calling startCrowdFund() --fail", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.token.call();
//         }).then(function (result) {
//             return assert.strictEqual(result, tokenAddress);
//         }).then(function(result){
//             return crowdsale.changeCrowdfundState({from : founderMultiSigAddress , gas:150000});
//         }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });


//       it(" startFirstPreSale : first presale should start", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.isFirstPreSaleActive.call();
//         }).then(function(result){
//             assert.isTrue(result);
//             done();
//         }).catch(done); 
//     });


//      it(" startFirstPreSale : first presale should start -- fails called by another account not by founder", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: account[8],gas:150000});
//         }).then(function(result){
//             return crowdsale.isFirstPreSaleActive.call();
//          }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });


//       it(" startFirstPreSale : first presale should start -- fails token is not deployed", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         }).then(function (result) {
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: account[8],gas:150000});
//         }).then(function(result){
//             return crowdsale.isFirstPreSaleActive.call();
//          }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });

//      it(" startFirstPreSale : first presale should start --fails call again ", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.isFirstPreSaleActive.call();
//         }).then(function(result){
//             return assert.isTrue(result);
//         }).then(function(result){
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });



//       it(" startSecondPreSale : second presale should start", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.startSecondPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.isSecondPreSaleActive.call();
//         }).then(function(result){
//             assert.isTrue(result);
//             done();
//         }).catch(done); 
//     });


//      it(" startSecondPreSale : second presale should start -- fails called by another account not by founder", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.startSecondPreSale(15 , 1000 , 20,{from: account[8],gas:150000});
//         }).then(function(result){
//             return crowdsale.isSecondPreSaleActive.call();
//          }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });


//       it(" startSecondPreSale : second presale should start -- fails token is not deployed", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         }).then(function (result) {
//             return crowdsale.startSecondPreSale(15 , 1000 , 20,{from: account[8],gas:150000});
//         }).then(function(result){
//             return crowdsale.isSecondPreSaleActive.call();
//          }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });

//      it(" startSecondPreSale : second presale should start --fails call again ", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.startSecondPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.isSecondPreSaleActive.call();
//         }).then(function(result){
//             return assert.isTrue(result);
//         }).then(function(result){
//             return crowdsale.startSecondPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });

//      it(" startSecondPreSale : second presale should start --fails because first presale is running ", function(done) {
//          var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.isFirstPreSaleActive.call();
//         }).then(function(result){
//             return assert.isTrue(result);
//         }).then(function(result){
//             return crowdsale.startSecondPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });

//     it("buyTokens : buy tokens at first presale",function(done){
//         var crowdsale;
//         var token;
//         var initialBalance = web3.eth.getBalance(founderMultiSigAddress);
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//             crowdsale = instance;
//             QtmToken.new(founderMultiSigAddress,crowdsale.address,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
//                  token = instance;
//                 return token;
//         }).then(function(result){
//             return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//                     return new Promise((resolve, reject) => {
//                         web3.currentProvider.sendAsync({
//                             jsonrpc: "2.0",
//                             method: "evm_increaseTime",
//                             params: [(500)] // 500 seconds after the start of crowdfund 
//                         }, function (err, result) {
//                             if (err) {
//                                 reject(err);
//                             }
//                             resolve(result);
//                         });
//                     });
//         }).then(function(result){
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(5).times(new BigNumber(10).pow(18))});
//         }).then(function (result) {
//             return web3.eth.getBalance(founderMultiSigAddress);
//         }).then(function (result) {
//                 assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 5, 0.16);
//                 return token.balanceOf.call(accounts[8]);
//         }).then(function(balance){
//                 assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 6000);  // 2 week bonus 14 %
//                 return token.totalAllocatedTokens.call();
//         }).then(function(result){
//                 assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),6000);
//             done();  
//         }).catch(done);
//     });
// });



// it("buyTokens : buy tokens at first presale -- fail because of the time expiration",function(done){
//         var crowdsale;
//         var token;
//         var initialBalance = web3.eth.getBalance(founderMultiSigAddress);
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//             crowdsale = instance;
//             QtmToken.new(founderMultiSigAddress,crowdsale.address,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
//                  token = instance;
//                 return token;
//         }).then(function(result){
//             return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//                     return new Promise((resolve, reject) => {
//                         web3.currentProvider.sendAsync({
//                             jsonrpc: "2.0",
//                             method: "evm_increaseTime",
//                             params: [(500)] // 500 seconds after the start of crowdfund 
//                         }, function (err, result) {
//                             if (err) {
//                                 reject(err);
//                             }
//                             resolve(result);
//                         });
//                     });
//         }).then(function(result){
//             return crowdsale.startFirstPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(5).times(new BigNumber(10).pow(18))});
//         }).then(function (result) {
//             return web3.eth.getBalance(founderMultiSigAddress);
//         }).then(function (result) {
//                 assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 5, 0.16);
//                 return token.balanceOf.call(accounts[8]);
//         }).then(function(balance){
//                 assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 6000);  // 2 week bonus 14 %
//                 return token.totalAllocatedTokens.call();
//         }).then(function(result){
//                return assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),6000);
//         }).then(function(result){
//               return new Promise((resolve, reject) => {
//                         web3.currentProvider.sendAsync({
//                             jsonrpc: "2.0",
//                             method: "evm_increaseTime",
//                             params: [(16*24*60*60 + 1000)] // 1000 seconds after end of first presale 
//                         }, function (err, result) {
//                             if (err) {
//                                 reject(err);
//                             }
//                             resolve(result);
//                         });
//                     });
//         }).then(function(result){
//              return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(5).times(new BigNumber(10).pow(18))});
//         }).catch(() => {
//             assert(true, true);
//             done();
//         });
//     });
// });



//  it("buyTokens : buy tokens at second presale",function(done){
//         var crowdsale;
//         var token;
//         var initialBalance = web3.eth.getBalance(founderMultiSigAddress);
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//             crowdsale = instance;
//             QtmToken.new(founderMultiSigAddress,crowdsale.address,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
//                  token = instance;
//                 return token;
//         }).then(function(result){
//             return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//                     return new Promise((resolve, reject) => {
//                         web3.currentProvider.sendAsync({
//                             jsonrpc: "2.0",
//                             method: "evm_increaseTime",
//                             params: [(500)] // 500 seconds after the start of crowdfund 
//                         }, function (err, result) {
//                             if (err) {
//                                 reject(err);
//                             }
//                             resolve(result);
//                         });
//                     });
//         }).then(function(result){
//             return crowdsale.startSecondPreSale(15 , 1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(5).times(new BigNumber(10).pow(18))});
//         }).then(function (result) {
//             return web3.eth.getBalance(founderMultiSigAddress);
//         }).then(function (result) {
//                 assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 5, 0.16);
//                 return token.balanceOf.call(accounts[8]);
//         }).then(function(balance){
//                 assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 6000);  // 2 week bonus 14 %
//                 return token.totalAllocatedTokens.call();
//         }).then(function(result){
//                 assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),6000);
//             done();  
//         }).catch(done);
//     });
// });



// it("buyTokens : buy tokens at crowdsale",function(done){
//         var crowdsale;
//         var token;
//         var initialBalance = web3.eth.getBalance(founderMultiSigAddress);
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//             crowdsale = instance;
//             QtmToken.new(founderMultiSigAddress,crowdsale.address,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
//                  token = instance;
//                 return token;
//         }).then(function(result){
//             return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//                     return new Promise((resolve, reject) => {
//                         web3.currentProvider.sendAsync({
//                             jsonrpc: "2.0",
//                             method: "evm_increaseTime",
//                             params: [(500)] // 500 seconds
//                         }, function (err, result) {
//                             if (err) {
//                                 reject(err);
//                             }
//                             resolve(result);
//                         });
//                     });
//         }).then(function(result){
//             return crowdsale.startCrowdfund(1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//              return new Promise((resolve, reject) => {
//                         web3.currentProvider.sendAsync({
//                             jsonrpc: "2.0",
//                             method: "evm_increaseTime",
//                             params: [(500)] // 500 seconds after the start of crowdfund 
//                         }, function (err, result) {
//                             if (err) {
//                                 reject(err);
//                             }
//                             resolve(result);
//                         });
//                     });
//         }).then(function(result){
//             return crowdsale.setBonus(40,35,30,25,20,15,10,{from:founderMultiSigAddress,gas:150000});
//         }).then(function(result){
//             return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(5).times(new BigNumber(10).pow(18))});
//         }).then(function (result) {
//             return web3.eth.getBalance(founderMultiSigAddress);
//         }).then(function (result) {
//                 assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 5, 0.16);
//                 return token.balanceOf.call(accounts[8]);
//         }).then(function(balance){
//                 assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 7000);  // 2 week bonus 14 %
//                 return token.totalAllocatedTokens.call();
//         }).then(function(result){
//                 assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),7000);
//             done();  
//         }).catch(done);
//     });
// });





//  it("buyTokens: should Not let buy tokens -- crowdfund is done", function(done) {
//         var crowdsale;
//         var token;
//         var now = Date.now();
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         QtmToken.new(founderMultiSigAddress,crowdsale.address,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
//         token = instanceTo;
//         return token;
//         }).then(function (result) {
//             return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddress, gas:150000});
//         }).then(function (result) {
//                     return new Promise((resolve, reject) => {
//                         web3.currentProvider.sendAsync({
//                             jsonrpc: "2.0",
//                             method: "evm_increaseTime",
//                             params: [(500)] // 500 seconds
//                         }, function (err, result) {
//                             if (err) {
//                                 reject(err);
//                             }
//                             resolve(result);
//                         });
//                     });
//         }).then(function (result) {
//             return crowdsale.startCrowdfund(1000 , 20,{from: founderMultiSigAddress,gas:150000});
//         }).then(function (result) {
//             return new Promise((resolve,reject) => {
//                 web3.currentProvider.sendAsync({
//                 jsonrpc: "2.0",
//                 method: "evm_increaseTime",
//                 params: [(500)] // 200 seconds after 
//             }, function(err, result) {
//                 if(err) {
//                     reject(err);
//                 }
//                 resolve(result);
//                 });
//             });
//         }).then(function(result){
//             return crowdsale.setBonus(40,35,30,25,20,15,10,{from:founderMultiSigAddress,gas:150000});
//         }).then(function (result) {
//             return new Promise((resolve,reject) => {
//                 web3.currentProvider.sendAsync({
//                 jsonrpc: "2.0",
//                 method: "evm_increaseTime",
//                 params: [(15*24*60*60 + 500)] // 500 seconds after the crowdfund ended
//             }, function(err, result) {
//                 if(err) {
//                     reject(err);
//                 }
//                 resolve(result);
//                 });
//             });
//         }).then(function(result){
//             return crowdsale.buyTokens(accounts[8], {value: new BigNumber(1).times(new BigNumber(10).pow(18)), from: accounts[8]});
//         }).catch(function (result) {
//             assert(true, true)
//             done();
//         });
//     });
//  });





// it("buyTokens: should Not let buy tokens -- crowdfund is not active", function(done) {
//         var crowdsale;
//         var token;
//         var now = Date.now();
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         QtmToken.new(founderMultiSigAddress,crowdsale.address,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
//         token = instance;
//         return token;
//         }).then(function (result) {
//             return token.setTokenAddress(result.address, {from: founderMultiSigAddress});
//         }).then(function (result) {
//             return crowdsale.buyTokens(accounts[8], {value: new BigNumber(2).times(new BigNumber(10).pow(18)), from: accounts[8]});
//         }).catch(function (result) {
//             assert(true, true)
//             done();
//         }).catch(done);
//     });
// });

// it("function(): should throw on an undetermined call", function(done) {
//        var crowdsale;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//             return crowdsale.call();
//         }).catch(function (result) {
//             assert.equal(true, true);
//             done();
//         })
//     });

// //  // Test case to check the functionality of close crowdFund

// it("closeCrowdFund : close crowd fund and send tokens to remaining token holder ",function(done){
//     var crowdsale;
//     var token;
//     var intialBalance;
//         QtmCrowdFund.new(founderMultiSigAddress,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
//         crowdsale = instance;
//         QtmToken.new(founderMultiSigAddress,crowdsale.address,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
//         token = instance;
//         return token;
//     }).then(function(result){
//         return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddress, gas:150000});
//     }).then(function(result){
//         return crowdsale.startCrowdfund(1000,20,{from: founderMultiSigAddress, gas:150000});
//     }).then(function(result){
//             return new Promise((resolve,reject) => {
//                 web3.currentProvider.sendAsync({
//                 jsonrpc: "2.0",
//                 method: "evm_increaseTime",
//                 params: [(200)] // 200 seconds after the crowdfund started
//             }, function(err, result) {
//                 if(err) {
//                     reject(err);
//                 }
//                 resolve(result);
//                 });
//             });
//         }).then(function(result){
//             return crowdsale.setBonus(40,35,30,25,20,15,10,{from:founderMultiSigAddress,gas:150000});
//         }).then(function (result) {
//             return new Promise((resolve,reject) => {
//                 web3.currentProvider.sendAsync({
//                 jsonrpc: "2.0",
//                 method: "evm_increaseTime",
//                 params: [(500)] // 500 seconds after the crowdfund ended
//             }, function(err, result) {
//                 if(err) {
//                     reject(err);
//                 }
//                 resolve(result);
//                 });
//             });
//       }).then(function (result) {
//          return crowdsale.buyTokens(accounts[8],{from: accounts[8], gas:2000000 , value: new BigNumber(1).times(new BigNumber(10).pow(18))});
//      }).then(function(result){
//             return new Promise((resolve,reject) => {
//                 web3.currentProvider.sendAsync({
//                 jsonrpc: "2.0",
//                 method: "evm_increaseTime",
//                 params: [(20*24*60*60 + 200)] // 200 seconds after the crowdfund ended
//             }, function(err, result) {
//                 if(err) {
//                     reject(err);
//                 }
//                 resolve(result);
//                 });
//             });
//          }).then(function(result){
//          return token.balanceOf.call(reaminingTokenHolder);
//          }).then(function(balance){
//         return assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(),0);    
//         }).then(function(result){
//              return crowdsale.endCrowdfund({from:founderMultiSigAddress , gas:1500000});
//         }).then(function(result){
//             return token.balanceOf.call(reaminingTokenHolder);
//         }).then(function(balance){
//            assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(),499998600); //remaining fund
//             done(); 
//         }).catch(done);
// });
// });

 });