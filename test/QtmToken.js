var QtmToken = artifacts.require("./QtmToken.sol");
var BigNumber = require('bignumber.js');

contract(QtmToken , function(accounts){
var founderMultiSigAddress = accounts[1];
var advisorAddress = accounts[3];
var vestingFounderAddress = accounts[4];
var crowdsaleAddress = accounts[2];

// creation of token with rewardHolder , founderAddress , advisorAddress,foundersWalletAddressWithHold,owner . Below test cases are to ckeck the functionality of constructor function

it("QtmToken : should create new instance ", function(done){
        var token;
        QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
        return token.balanceOf.call(crowdsaleAddress); 
        }).then(function(rewardHolderBalance){
            assert.strictEqual(rewardHolderBalance.dividedBy(new BigNumber(10).pow(18)).toNumber(),500000000);
           done();
        }).catch(done); 
    });

it("totalAllocatedTokens: total Allocated tokens should be equal to 0 ",function(done){
    QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
    return  instance.totalAllocatedTokens.call();
    }).then(function (result){
             assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),0);
              done();
        }).catch(done);
});


// // Test cases for changeFounderMultiSigAddress 

it("changeFounderMultiSigAddress : should return true",function(done){
    var token;
      QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
        result.changeFounderMultiSigAddress(accounts[6],{from : founderMultiSigAddress , gas:1500000}).then(function(tx){
            return tx;
        }).then(function(result){
            return token.founderMultiSigAddress.call();
        }).then(function(account){
            assert.strictEqual(account,accounts[6]);
            done();
        }).catch(done);
    });
});

it("changeFounderMultiSigAddress : should return an exception when msg.sender other than founder",function(){
    var token;
      QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
        result.changeFounderMultiSigAddress(accounts[6],{from : accounts[7] , gas:1500000}).then(function(tx){
        return tx;
    }).then(function(result){
        assert(false, "it expected to return exception but it didn't");
    }).catch(function(error){
        if(error.toString().indexOf("invalid opcode") != -1) {
         console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to check the call other than founder");
         assert(true,true);
         }else{
         assert(false,error.toString());
        }
    });
  });
});


// // // Test cases to function transferRewardToken 

it("transferRewardToken: should transfer MarketIncentivesFund to accounts[8]", function(done){
 var token;
     QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
        result.transferRewardToken(accounts[8],200000,{from : founderMultiSigAddress , gas:1500000}).then(function(tx){
        return tx;
    }).then(function(resullt){
        return token.balanceOf.call(accounts[8]);
    }).then(function(balance){
        assert.strictEqual(balance.toNumber(),200000);
        done();
    }).catch(done);
});
});


it("transferRewardToken: should transfer 200000 to account[8] , it doesn't call by founder -- Error", function(){
     var token;
     QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
        return instance;
    }).then(function(result){
        token.transferRewardToken(accounts[8],200000,{from : accounts[7] , gas:1500000}).then(function(tx){
        return tx;
    }).then(function(resullt){
         assert(false, "it expected to return exception but it didn't");
    }).catch(function(error){
        if(error.toString().indexOf("invalid opcode") != -1) {
         console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to check the call other than founder");
         assert(true,true);
         }else{
         assert(false,error.toString());
        }
    });
});
});

 // Test case to check the functionality of fallback function

it("transfer: ether directly to the token contract -- it will throw", function(){
    var token;
   QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
       return web3.eth.sendTransaction({from: accounts[8], to: token.address, value: web3.toWei("10", "Ether")});
    }).catch(function(error){
        if(error.toString().indexOf("invalid opcode") != -1) {
         console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to check the fallback functionality");
         assert(true,true);
         }else{
         assert(false,error.toString());
        }
    });
});


 // test case to check the functionality of transfer function

it("transfer: should transfer 10000 to accounts[8] from founders accounts", function(done) {
        var token;
       QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
            return token.transfer(accounts[8], 10000, {from: crowdsaleAddress});
        }).then(function (result) {
            return token.balanceOf.call(accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 10000);
            done();
        }).catch(done);
    });



   it("transfer: should fail when trying to transfer zero.", function() {
        var token;
      QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
        return token.transfer.call(accounts[8], 0, {from: crowdsaleAddress});
        }).then(function (result) {
           assert(false, "it expected to return exception but it didn't");
        }).catch(function(error){
        if(error.toString().indexOf("invalid opcode") != -1) {
         console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to fail when trying to transfer zero.");
         assert(true,true);
         }else{
         assert(false,error.toString());
        }
    });
});



    // to check the functionality of approve , allowed 

     it("approve: msg.sender should approve 1000 to accounts[8]", function(done) {
        var token;
        QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
        return token.approve(accounts[8], 1000, {from: crowdsaleAddress});
        }).then(function (result) {
            return token.allowance.call(crowdsaleAddress , accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 1000);
            done();
        }).catch(done);
    });


     it("approve: msg.sender approves accounts[8] of 1000 & withdraws 100 once.", function(done) {
        var token;
         QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
            return token.balanceOf.call(accounts[0]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 0);
            return token.transfer(accounts[0], 10000, {from: crowdsaleAddress});
        }).then(function (result) {
            return token.balanceOf.call(accounts[0]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 10000);
            return token.approve(accounts[8], 1000, {from: accounts[0]});
        }).then(function (result) {
            return token.balanceOf.call(accounts[7]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 0);
            return token.allowance.call(accounts[0], accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 1000);
            return token.transferFrom.call(accounts[0], accounts[7], 100, {from: accounts[8]});
        }).then(function (result) {
            return token.transferFrom(accounts[0], accounts[7], 100, {from: accounts[8]});
        }).then(function (result) {
            return token.allowance.call(accounts[0], accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 900);
            return token.balanceOf.call(accounts[7]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 100);
            return token.balanceOf.call(accounts[0]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 9900);
            done();
        }).catch(done);
    });


     it("approves : msg.sender approves accounts[8] of 1000 & withdraws 100 twice.", function(done) {
         var token;
         QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
            return token.approve(accounts[8], new BigNumber(1000).times(new BigNumber(10).pow(18)), {from: crowdsaleAddress});
        }).then(function (result) {
            return token.allowance.call(crowdsaleAddress, accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 1000);
            return token.transferFrom(crowdsaleAddress, accounts[7], new BigNumber(100).times(new BigNumber(10).pow(18)).toNumber(), {from: accounts[8]});
        }).then(function (result) {
            return token.allowance.call(crowdsaleAddress, accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 900);
            return token.balanceOf.call(accounts[7]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 100);
            return token.balanceOf.call(crowdsaleAddress);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 499999900);
            return token.transferFrom(crowdsaleAddress, accounts[7], new BigNumber(100).times(new BigNumber(10).pow(18)).toNumber(), {from: accounts[8]});
        }).then(function (result) {
            return token.allowance.call(crowdsaleAddress, accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 800);
            return token.balanceOf.call(accounts[7]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 200);
            return token.balanceOf.call(crowdsaleAddress);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 499999800);
            done();
        }).catch(done);
    });

    it("Approve max (2^256 - 1)", function(done) {
        var token;
         QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
            return token.approve(accounts[8],'115792089237316195423570985008687907853269984665640564039457584007913129639935' , {from: accounts[7]});
        }).then(function (result) {
            return token.allowance(accounts[7], accounts[8]);
        }).then(function (result) {
            var status = result.equals('1.15792089237316195423570985008687907853269984665640564039457584007913129639935e+77');
            assert.isTrue(status);
            done();
        }).catch(done);
    });

    it("approves: msg.sender approves accounts[8] of 1000 & withdraws 800 & 500 (2nd tx should fail)", function(done) {
        var token;
         QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
            return token.approve(accounts[8], new BigNumber(1000).times(new BigNumber(10).pow(18)), {from: crowdsaleAddress});
        }).then(function (result) {
            return token.allowance(crowdsaleAddress, accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 1000);
            return token.transferFrom(crowdsaleAddress, accounts[7], new BigNumber(800).times(new BigNumber(10).pow(18)), {from: accounts[8]});
        }).then(function (result) {
            return token.allowance(crowdsaleAddress, accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 200);
            return token.balanceOf(accounts[7]);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 800);
            return token.balanceOf(crowdsaleAddress);
        }).then(function (result) {
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(), 499999200);
            return token.transferFrom.call(crowdsaleAddress, accounts[7], new BigNumber(500).times(new BigNumber(10).pow(18)), {from: accounts[8]});
        }).then(function (result) {
            assert.isFalse(result);
            done();   
        }).catch(done);
    });


     it("transferFrom: Attempt to  withdraw from account with no allowance  -- fail", function(done) {
        var token;
        QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
            return token.transferFrom.call(accounts[7], founderMultiSigAddress ,100, {from: accounts[8]});
        }).then(function (result) {
            assert.isFalse(result); 
            done();
        }).catch(done); 
    });


     it("transferFrom: Allow accounts[8] 1000 to withdraw from accounts[7]. Withdraw 600 and then approve 0 & attempt transfer.", function(done) {
        var token;
         QtmToken.new(founderMultiSigAddress,crowdsaleAddress,{from : founderMultiSigAddress , gas:2000000}).then(function(instance){
        token = instance;
            return token.approve(accounts[8], 1000, {from: accounts[7]});
        }).then(function (result) {
            return token.transferFrom(accounts[7], crowdsaleAddress, 600, {from: accounts[8]});
        }).then(function (result) {
            return token.approve(accounts[8], 0, {from: accounts[7]});
        }).then(function (result) {
            return token.transferFrom.call(accounts[7],crowdsaleAddress , 10, {from: accounts[8]});
        }).then(function (result) {
         assert.isFalse(result); 
            done();
        }).catch(done); 
    });


 });



