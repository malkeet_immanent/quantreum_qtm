pragma solidity ^0.4.15;

import './helpers/BasicToken.sol';
import './lib/safeMath.sol';

contract QtmToken is BasicToken {

using SafeMath for uint256;

//token attributes

string public name = "Quantreum";                 //name of the token

string public symbol = "QTM";                      // symbol of the token

uint8 public decimals = 18;                        // decimals

uint256 public totalSupply = 300000000 * 10**18;  // total supply of Qtm Tokens 

uint256 public totalAllocatedTokens;                // variable to regulate the funds allocation

uint256 public tokensForCrowdsale;                  // Tokens for ICO (crowdsale) 

uint256 public tokensForMining;                  // Tokens for Mining 

uint256 public tokensForCompany;                  // Tokens for Company

uint256 public tokensForDeveloper;                  // Tokens for Developer

// addresses

address public manageCrowdSaleAddress;              // manage CrowdSale Address

address public crowdsaleAddress;                    // address of crowdsale contract

// Address to allocate the mining
address public miningAddress = 0x4C7f3EcE04030467551346A724ef38cE0C8Cd4F2;
// Address to allocate the company
address public companyAddress = 0xF6e9c48737caE03ebe46bE67A19e013EBf33Aef3;
// Address to allocate the develper
address public developerAddress = 0x0574800C1f198f310973682f13bfAf48407961ac;      


//events
event ChangeFoundersWalletAddress(uint256  _blockTimeStamp, address indexed _foundersWalletAddress);


///////////////////////////////////////// CONSTRUCTOR //////////////////////////////////////////////////

   function QtmToken (address _manageCrowdSaleAddress, address _crowdsaleContract) {
    
    manageCrowdSaleAddress = _manageCrowdSaleAddress;
    crowdsaleAddress = _crowdsaleContract;
                                            
    tokensForMining = 105000000 * 10**18;                 // 35 % allocation of totalSupply    
    tokensForCompany = 60000000 * 10**18;                 // 20 % allocation of totalSupply
    tokensForDeveloper = 30000000 * 10**18;               // 10 % allocation of totalSupply     
    tokensForCrowdsale = 105000000 * 10**18;              // 35 % allocation of totalSupply

    balances[miningAddress] = tokensForMining;
    balances[companyAddress] = tokensForCompany;
    balances[developerAddress] = tokensForDeveloper;
    balances[crowdsaleAddress] = tokensForCrowdsale;

  }

///////////////////////////////////////// MODIFIERS /////////////////////////////////////////////////

  modifier onlyCrowdsale() {
    require(msg.sender == crowdsaleAddress);
    _;
  }

  modifier nonZeroAddress(address _to) {
    require(_to != 0x0);
    _;
  }

  modifier onlyFounders() {
    require(msg.sender == manageCrowdSaleAddress);
    _;
  }

////////////////////////////////////////// FUNCTIONS //////////////////////////////////////////////

  /**
      @dev function is used to change the founder wallet address called only by founder
      @param _newManageCrowdSaleAddress new address of founder
  
   */
           
  function changeManageCrowdSaleAddress(address _newManageCrowdSaleAddress) 
  onlyFounders 
  nonZeroAddress(_newManageCrowdSaleAddress) 
  {
    manageCrowdSaleAddress = _newManageCrowdSaleAddress;
    ChangeFoundersWalletAddress(now, manageCrowdSaleAddress);
  }

}